using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Animator animator;
    [SerializeField] private float health = 100f;
    [SerializeField] private float damage = 20f;
    [SerializeField] private float runningDistance = 10f;
    [SerializeField] private float stoppingDistance = 1.5f;
    [SerializeField] private float walkingDistance = 4f;

    private bool isDead = false;

    private void Awake()
    {
        InitializeComponents();
    }

    private void InitializeComponents()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = stoppingDistance;
    }

    private void Update()
    {
        if (!isDead)
        {
            ManageEnemyBehavior();
        }
    }

    private void ManageEnemyBehavior()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        agent.destination = player.transform.position;

        if (distanceToPlayer <= stoppingDistance)
        {
            agent.speed = 0f;
            SetAnimatorBooleans(false, false);
        }
        else if (distanceToPlayer <= walkingDistance)
        {
            agent.speed = 0.8f;
            SetAnimatorBooleans(false, true);
        }
        else if (distanceToPlayer <= runningDistance)
        {
            agent.speed = 5f;
            SetAnimatorBooleans(true, false);
        }
        else
        {
            agent.speed = 0.8f;
            SetAnimatorBooleans(false, true);
        }
    }

    private void SetAnimatorBooleans(bool isRunning, bool isWalking)
    {
        animator.SetBool("isRunning", isRunning);
        animator.SetBool("isWalking", isWalking);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Damage();
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Attacking");
            agent.speed = 0;
            animator.SetBool("isAttacking", true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            agent.speed = 0.8f;
            animator.SetBool("isAttacking", false);
        }
    }

    private void Damage()
    {
        health -= damage;
        if (health <= 0 && !isDead)
        {
            isDead = true;
            StartCoroutine(Die());
        }
    }

    private IEnumerator Die()
    {
        animator.SetBool("isDead", true);
        DisableComponents();
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }

    private void DisableComponents()
    {
        agent.enabled = false;
        GetComponent<Collider>().enabled = false;
    }
}
